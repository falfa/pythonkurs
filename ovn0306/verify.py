#!/usr/bin/python3
# coding=utf-8

import pytest
import io, sys, re
from importlib import import_module

from contextlib import redirect_stdout

name = "frekvent"


def test_avsluta_med_blankrad():
    f = io.StringIO()
    if name in sys.modules: del sys.modules[name]
    with redirect_stdout(f):
        sys.stdin = io.StringIO("hej och hå\r\n\r\n")
        import_module(name)


def test_frekvent():
    f = io.StringIO()
    if name in sys.modules: del sys.modules[name]
    with redirect_stdout(f):
        sys.stdin = io.StringIO("ett och två\r\nsamt tre och fyra\r\n\r\n")
        import_module(name)

    res = f.getvalue()

    assert re.search(r'\boch\b', res), \
        "Resultatet 'och' förväntades"


def test_radbrytning():
    f = io.StringIO()
    if name in sys.modules: del sys.modules[name]
    with redirect_stdout(f):
        sys.stdin = io.StringIO("tal ett ett\r\nett och tal tre\r\n\r\n")
        import_module(name)

    res = f.getvalue()

    assert re.search(r'\bett\b', res), \
        "Resultatet 'ett' förväntades: tal ett ett\nett och tal tre"


def test_dubbletter():
    f = io.StringIO()
    if name in sys.modules: del sys.modules[name]
    with redirect_stdout(f):
        sys.stdin = io.StringIO("tal ett och\r\nett och annat tal\r\n\r\n")
        import_module(name)

    res = f.getvalue()

    assert re.search(r'\bett\b', res), \
        "Resultatet 'ett' förväntades: tal ett och\nett och annat tal"
